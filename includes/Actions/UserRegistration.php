<?php

use DialogContactForm\Abstracts\Action;
use DialogContactForm\Supports\Config;
use DialogContactForm\Supports\Mailer;
use DialogContactForm\Supports\Utils;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class UserRegistration extends Action {

	/**
	 * UserRegistration constructor.
	 */
	public function __construct() {
		$this->priority   = 15;
		$this->id         = 'user_registration';
		$this->title      = __( 'User Registration', 'dialog-contact-form' );
		$this->meta_group = 'user_registration';
		$this->meta_key   = '_action_user_registration';
		$this->settings   = array_merge( $this->settings, $this->settings() );
	}

	/**
	 * Process current action
	 *
	 * @param Config $config Contact form configurations
	 * @param array $data User submitted sanitized data
	 *
	 * @return mixed
	 */
	public static function process( $config, $data ) {

		// Registration closed
		if ( ! get_option( 'users_can_register' ) ) {
			return false;
		}

		$messages                       = $config->getValidationMessages();
		$messages['email_exists']       = Utils::get_option( 'email_exists',
			__( 'An account exists with this email address.', 'dialog-contact-form' ) );
		$messages['username_exists']    = Utils::get_option( 'username_exists',
			__( 'An account exists with this username.', 'dialog-contact-form' ) );
		$messages['invalid_username']   = Utils::get_option( 'invalid_username',
			__( 'Sorry, the username you entered is not valid.', 'dialog-contact-form' ) );
		$messages['username_too_short'] = Utils::get_option( 'username_too_short',
			__( 'Username too short. At least 4 characters is required.', 'dialog-contact-form' ) );

		$action_settings = get_post_meta( $config->getFormId(), '_action_user_registration', true );

		$user_email = isset( $data[ $action_settings['user_email'] ] ) ? $data[ $action_settings['user_email'] ] : null;
		$user_login = isset( $data[ $action_settings['user_login'] ] ) ? $data[ $action_settings['user_login'] ] : null;

		$errors = array();
		if ( ! is_email( $user_email ) ) {
			$errors[ $action_settings['user_email'] ][] = $messages['invalid_required'];
		}

		if ( username_exists( $user_email ) || email_exists( $user_email ) ) {
			$errors[ $action_settings['user_email'] ][] = $messages['email_exists'];
		}

		// Validate Username
		if ( ! empty( $action_settings['user_login'] ) ) {
			// Check if username exists
			if ( username_exists( $user_login ) || email_exists( $user_login ) ) {
				$errors[ $action_settings['user_login'] ][] = $messages['username_exists'];
			}

			// Check if username has minimum length
			if ( 4 > strlen( $user_login ) ) {
				$errors[ $action_settings['user_login'] ][] = $messages['username_too_short'];
			}

			if ( ! validate_username( $user_login ) ) {
				$errors[ $action_settings['user_login'] ][] = $messages['invalid_username'];
			}
		}

		// Exit if there is any error
		if ( count( $errors ) > 0 ) {

			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
				wp_send_json( array(
					'status'     => 'fail',
					'message'    => $config->getValidationErrorMessage(),
					'validation' => $errors,
				), 422 );
			}

			$GLOBALS['_dcf_errors']           = $errors;
			$GLOBALS['_dcf_validation_error'] = $config->getValidationErrorMessage();

			return false;
		}

		$user_data = array(
			'user_email' => $data[ $action_settings['user_email'] ],
		);

		// User Login
		if ( ! empty( $action_settings['user_login'] ) ) {
			$user_data['user_login'] = $data[ $action_settings['user_login'] ];
		} else {
			$user_data['user_login'] = $data[ $action_settings['user_email'] ];
		}

		// Website
		if ( ! empty( $action_settings['user_url'] ) ) {
			$user_data['user_url'] = $data[ $action_settings['user_url'] ];
		}

		// First name
		if ( ! empty( $action_settings['first_name'] ) ) {
			$user_data['first_name'] = $data[ $action_settings['first_name'] ];
		}

		// Last name
		if ( ! empty( $action_settings['last_name'] ) ) {
			$user_data['last_name'] = $data[ $action_settings['last_name'] ];
		}

		// Description
		if ( ! empty( $action_settings['description'] ) ) {
			$user_data['description'] = $data[ $action_settings['description'] ];
		}

		// Generate the password so that the subscriber will have to check email...
		$user_data['user_pass'] = wp_generate_password();

		$user_id = wp_insert_user( $user_data );

		if ( ! is_wp_error( $user_id ) ) {
			if ( 'manual' !== $action_settings['user_activation_method'] ) {
				self::sendEmailToUser( $user_id );
			}
			if ( 'no' !== $action_settings['send_email_to_admin'] ) {
				self::sendEmailToAdmin( $user_id );
			}

			return $user_id;
		}

		return false;
	}

	/**
	 * Send Email to new user
	 *
	 * @param int $user_id
	 */
	private static function sendEmailToUser( $user_id ) {
		$site_name = get_bloginfo( 'name' );
		$user      = get_userdata( $user_id );
		$reset_url = add_query_arg( array(
			'action' => 'rp',
			'key'    => get_password_reset_key( $user ),
			'login'  => rawurlencode( $user->user_login ),
		), network_site_url( 'wp-login.php' ) );

		ob_start();
		$introLines = array(
			sprintf( __( 'Welcome to %s! Thanks so much for joining us. You provided the following information when signing up.',
				'dialog-contact-form' ), $site_name ),
			sprintf(
				__( '%1$sUsername:%2$s %4$s%3$s%1$sEmail:%2$s %5$s', 'dialog-contact-form' ),
				'<strong>',
				'</strong>',
				'<br>',
				$user->user_login,
				$user->user_email
			),
			__( 'To set your password, click on the following button:', 'dialog-contact-form' ),
		);
		$actionUrl  = $reset_url;
		$actionText = __( 'Set Password', 'dialog-contact-form' );
		require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/views/emails/email-wrapper.php';
		$user_message = ob_get_contents();
		ob_end_clean();
		$user_subject = sprintf( __( 'Welcome to %s', 'dialog-contact-form' ), $site_name );

		$mailer = new Mailer();
		$mailer->setSubject( $user_subject );
		$mailer->setMessage( $user_message );
		$mailer->setReceiver( $user->user_email );
		$mailer->setContentType( 'html' );
		$mailer->send();
	}

	/**
	 * Send email to admin
	 *
	 * @param $user_id
	 */
	private static function sendEmailToAdmin( $user_id ) {
		$user = get_userdata( $user_id );

		ob_start();
		$introLines = array(
			sprintf( __( 'A new user has been registered at %s%s%s using the following details.',
				'dialog-contact-form' ), '<strong>', get_bloginfo( 'name' ), '</strong>' ),
			sprintf(
				__( 'Email: %1$s %3$sUsername: %2$s', 'dialog-contact-form' ),
				$user->user_email,
				$user->user_login,
				'<br>'
			),
		);
		require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/views/emails/email-wrapper.php';
		$admin_message = ob_get_contents();
		ob_end_clean();

		$admin_subject = sprintf( __( '[%s] New User Registration', 'dialog-contact-form' ), get_bloginfo( 'name' ) );

		$mailer = new Mailer();
		$mailer->setSubject( $admin_subject );
		$mailer->setMessage( $admin_message );
		$mailer->setReceiver( get_option( 'admin_email' ) );
		$mailer->setContentType( 'html' );
		$mailer->send();
	}

	/**
	 * Get action description
	 *
	 * @return string
	 */
	public function getDescription() {
		// Registration closed, add error
		if ( ! ! get_option( 'users_can_register' ) ) {
			return '';
		}

		$html = '<p class="description">';
		$html .= sprintf(
			esc_html__( 'Registering new users is currently not allowed. To enable new user register, go to %1$sSettings --> General%2$s and check %3$s"Anyone can register"%4$s checkbox.',
				'dialog-contact-form' ),
			'<a href="' . esc_url( admin_url( 'options-general.php' ) ) . '">',
			'</a>',
			'<strong>',
			'</strong>' );
		$html .= '</p>';

		return $html;
	}

	/**
	 * Action settings
	 *
	 * @return array
	 */
	private function settings() {
		// Registration closed, not settings are available
		if ( ! get_option( 'users_can_register' ) ) {
			return array();
		}

		$config          = Config::init();
		$placeholder     = array( '' => __( '-- No Value --', 'dialog-contact-form' ) );
		$text_fields     = $placeholder;
		$textarea_fields = $placeholder;
		$url_fields      = $placeholder;
		$email_fields    = array();
		foreach ( $config->getFormFields() as $field ) {
			if ( empty( $field['field_type'] ) ) {
				continue;
			}
			if ( 'email' === $field['field_type'] ) {
				$email_fields[ $field['field_id'] ] = $field['field_title'];
			} elseif ( 'text' === $field['field_type'] ) {
				$text_fields[ $field['field_id'] ] = $field['field_title'];
			} elseif ( 'url' === $field['field_type'] ) {
				$url_fields[ $field['field_id'] ] = $field['field_title'];
			} elseif ( 'textarea' === $field['field_type'] ) {
				$textarea_fields[ $field['field_id'] ] = $field['field_title'];
			}
		}

		return array(
			'field_mapping'          => array(
				'type'  => 'section',
				'label' => __( 'Field Mapping', 'dialog-contact-form' ),
			),
			'user_email'             => array(
				'type'        => 'select',
				'id'          => 'user_email',
				'group'       => $this->meta_group,
				'meta_key'    => $this->meta_key,
				'label'       => __( 'Email Address *', 'dialog-contact-form' ),
				'description' => __( 'The user email address. This is required field.', 'dialog-contact-form' ),
				'options'     => $email_fields,
			),
			'user_login'             => array(
				'type'        => 'select',
				'id'          => 'user_login',
				'group'       => $this->meta_group,
				'meta_key'    => $this->meta_key,
				'label'       => __( 'Username', 'dialog-contact-form' ),
				'description' => __( 'To use email address as username, leave this field blank.',
					'dialog-contact-form' ),
				'options'     => $text_fields,
			),
			'first_name'             => array(
				'type'     => 'select',
				'id'       => 'first_name',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'First Name', 'dialog-contact-form' ),
				'options'  => $text_fields,
			),
			'last_name'              => array(
				'type'     => 'select',
				'id'       => 'last_name',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'Last Name', 'dialog-contact-form' ),
				'options'  => $text_fields,
			),
			'user_url'               => array(
				'type'     => 'select',
				'id'       => 'user_url',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'Website URL', 'dialog-contact-form' ),
				'options'  => $url_fields,
			),
			'description'            => array(
				'type'        => 'select',
				'id'          => 'description',
				'group'       => $this->meta_group,
				'meta_key'    => $this->meta_key,
				'label'       => __( 'Biographical Info', 'dialog-contact-form' ),
				'description' => __( 'The user\'s biographical description.', 'dialog-contact-form' ),
				'options'     => $textarea_fields,
			),
			'other_configuration'    => array(
				'type'  => 'section',
				'label' => __( 'Other Configuration', 'dialog-contact-form' ),
			),
			'user_activation_method' => array(
				'type'     => 'buttonset',
				'id'       => 'user_activation_method',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'User Activation Method', 'dialog-contact-form' ),
				'default'  => 'email',
				'options'  => array(
					'email'  => __( 'User Email', 'dialog-contact-form' ),
					'manual' => __( 'Manual Approve', 'dialog-contact-form' ),
				),
			),
			'send_email_to_admin'    => array(
				'type'     => 'buttonset',
				'id'       => 'send_email_to_admin',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'Send Email to Admin', 'dialog-contact-form' ),
				'default'  => 'no',
				'options'  => array(
					'yes' => __( 'Yes', 'dialog-contact-form' ),
					'no'  => __( 'No', 'dialog-contact-form' ),
				),
			),
		);
	}

	/**
	 * Check current request is AJAX
	 *
	 * @return bool
	 */
	private static function isAjax() {
		return defined( 'DOING_AJAX' ) && DOING_AJAX;
	}
}
