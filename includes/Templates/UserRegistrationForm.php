<?php

use DialogContactForm\Abstracts\Template;
use DialogContactForm\Supports\Utils;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class UserRegistrationForm extends Template {

	public function __construct() {
		$this->priority    = 200;
		$this->id          = 'user_registration';
		$this->title       = __( 'User Registration', 'dialog-contact-form' );
		$this->description = __( 'Register a WordPress user.', 'dialog-contact-form' );
	}

	/**
	 * Form fields
	 *
	 * @return array
	 */
	protected function formFields() {
		return array(
			array(
				'field_type'     => 'text',
				'field_title'    => __( 'First Name', 'dialog-contact-form' ),
				'field_id'       => 'first_name',
				'field_name'     => 'first_name',
				'required_field' => 'off',
				'field_width'    => 'is-6',
				'autocomplete'   => 'given-name',
				'placeholder'    => 'John',
			),
			array(
				'field_type'     => 'text',
				'field_title'    => __( 'Last Name', 'dialog-contact-form' ),
				'field_id'       => 'last_name',
				'field_name'     => 'last_name',
				'required_field' => 'off',
				'field_width'    => 'is-6',
				'autocomplete'   => 'family-name',
				'placeholder'    => 'Doe',
			),
			array(
				'field_type'     => 'email',
				'field_title'    => __( 'Email', 'dialog-contact-form' ),
				'field_id'       => 'email',
				'field_name'     => 'email',
				'required_field' => 'on',
				'field_width'    => 'is-6',
				'autocomplete'   => 'email',
				'placeholder'    => 'mail@example.com',
			),
			array(
				'field_type'     => 'text',
				'field_title'    => __( 'Username', 'dialog-contact-form' ),
				'field_id'       => 'username',
				'field_name'     => 'username',
				'required_field' => 'on',
				'field_width'    => 'is-6',
				'autocomplete'   => 'username',
			),
			array(
				'field_type'     => 'url',
				'field_title'    => __( 'Website URL', 'dialog-contact-form' ),
				'field_id'       => 'website_url',
				'field_name'     => 'website_url',
				'required_field' => 'off',
				'field_width'    => 'is-12',
				'autocomplete'   => 'url',
				'placeholder'    => 'https://example.com',
			),
		);
	}

	/**
	 * Form settings
	 *
	 * @return array
	 */
	protected function formSettings() {
		return array(
			'labelPosition' => 'both',
			'btnLabel'      => esc_html__( 'Register', 'dialog-contact-form' ),
			'btnAlign'      => 'left',
			'reset_form'    => 'yes',
			'recaptcha'     => 'no',
		);
	}

	/**
	 * Form actions
	 *
	 * @return array
	 */
	protected function formActions() {
		return array(
			'user_registration' => array(
				'user_email'             => 'email',
				'user_login'             => 'username',
				'first_name'             => 'first_name',
				'last_name'              => 'last_name',
				'user_url'               => 'website_url',
				'description'            => '',
				'user_activation_method' => 'email',
				'send_email_to_admin'    => 'no',
			),
			'success_message'   => array(
				'message' => __( 'Thank you for registering.', 'dialog-contact-form' ),
			),
			'redirect'          => array(
				'redirect_to' => 'same',
			),
		);
	}

	/**
	 * Form validation messages
	 *
	 * @return array
	 */
	protected function formValidationMessages() {
		return array(
			'mail_sent_ng'     => __( 'There was an error trying to submit the form. Please try again later.',
				'dialog-contact-form' ),
			'validation_error' => Utils::get_option( 'validation_error' ),
		);
	}
}
