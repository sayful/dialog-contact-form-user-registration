<?php
/**
 * Plugin Name: Dialog Contact Form - User Registration
 * Plugin URI: https://bitbucket.org/sayful/dialog-contact-form-user-registration/
 * Description: User Registration add-on for Dialog Contact Form WordPress plugin.
 * Version: 1.0.0
 * Author: Sayful Islam
 * Author URI: https://sayfulislam.com
 * Requires at least: 4.4
 * Tested up to: 4.9
 * Text Domain: dialog-contact-form-user-registration
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Dialog_Contact_Form_User_Registration' ) ) {

	class Dialog_Contact_Form_User_Registration {
		/**
		 * The instance of the class
		 *
		 * @var self
		 */
		private static $instance;

		/**
		 * Ensures only one instance of the class is loaded or can be loaded.
		 *
		 * @return self
		 */
		public static function init() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function __construct() {
			add_action( 'dialog_contact_form/actions', array( $this, 'add_action' ) );
			add_action( 'dialog_contact_form/templates', array( $this, 'add_template' ) );
			add_filter( 'dialog_contact_form/settings/fields', array( $this, 'add_settings_fields' ) );
		}

		/**
		 * Add our custom action to action manager
		 *
		 * @param \DialogContactForm\Collections\Actions $actions
		 */
		public function add_action( $actions ) {
			include_once 'includes/Actions/UserRegistration.php';

			$actions->set( 'user_registration', 'UserRegistration' );
		}

		/**
		 * Add our custom template to template manager
		 *
		 * @param \DialogContactForm\Collections\Templates $templates
		 */
		public function add_template( $templates ) {
			include_once 'includes/Templates/UserRegistrationForm.php';

			$templates->set( 'user_registration', 'UserRegistrationForm' );
		}

		/**
		 * Add our custom field validation messages
		 *
		 * @param array $fields
		 *
		 * @return array
		 */
		public function add_settings_fields( $fields ) {
			$fields[] = array(
				'id'       => 'email_exists',
				'type'     => 'textarea',
				'rows'     => 2,
				'name'     => __( 'Email exists', 'dialog-contact-form' ),
				'std'      => __( 'An account exists with this email address.', 'dialog-contact-form' ),
				'section'  => 'dcf_field_message_section',
				'priority' => 210,
			);
			$fields[] = array(
				'id'       => 'username_exists',
				'type'     => 'textarea',
				'rows'     => 2,
				'name'     => __( 'Username exists', 'dialog-contact-form' ),
				'std'      => __( 'An account exists with this username.', 'dialog-contact-form' ),
				'section'  => 'dcf_field_message_section',
				'priority' => 211,
			);
			$fields[] = array(
				'id'       => 'invalid_username',
				'type'     => 'textarea',
				'rows'     => 2,
				'name'     => __( 'Invalid username', 'dialog-contact-form' ),
				'std'      => __( 'Sorry, the username you entered is not valid.', 'dialog-contact-form' ),
				'section'  => 'dcf_field_message_section',
				'priority' => 212,
			);
			$fields[] = array(
				'id'       => 'username_too_short',
				'type'     => 'textarea',
				'rows'     => 2,
				'name'     => __( 'Username too short', 'dialog-contact-form' ),
				'std'      => __( 'Username too short. At least 4 characters is required.', 'dialog-contact-form' ),
				'section'  => 'dcf_field_message_section',
				'priority' => 213,
			);

			return $fields;
		}
	}
}

Dialog_Contact_Form_User_Registration::init();
